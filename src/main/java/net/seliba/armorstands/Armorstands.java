package net.seliba.armorstands;

import net.seliba.armorstands.utils.ItemStackBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.EulerAngle;

public class Armorstands extends JavaPlugin {

    private ArmorStand armorStand;
    private boolean currentState = false;
    private final EulerAngle ANGLE_1 = new EulerAngle(30.0, 90.0, 90.0);
    private final EulerAngle ANGLE_2 = new EulerAngle(30.0, 90.0, 85.0);

    @Override
    public void onEnable() {
        Bukkit.getConsoleSender().sendMessage("[Armorstands] Gestartet!");

        createAnimatedArmorstand();
    }

    @Override
    public void onDisable() {
        this.armorStand.remove();
    }

    private void createAnimatedArmorstand() {
        //Spawn Armorstand
        this.armorStand = (ArmorStand) Bukkit.getWorld("lobby").spawnEntity(new Location(Bukkit.getWorld("Lobby-New"), 3.0, 23.5, 0.0), EntityType.ARMOR_STAND);

        //Configure the Armorstand
        this.armorStand.setArms(true);
        this.armorStand.setInvulnerable(true);
        this.armorStand.setBasePlate(false);
        this.armorStand.setHeadPose(new EulerAngle(0.0, 10.0, 0.0));
        this.armorStand.setItem(EquipmentSlot.HAND, new ItemStackBuilder(Material.COMMAND_BLOCK).build());
        this.armorStand.setHelmet(new ItemStackBuilder(Material.PLAYER_HEAD).setOwner("Steve").build());
        this.armorStand.setChestplate(new ItemStackBuilder(Material.GOLDEN_CHESTPLATE).build());
        this.armorStand.setLeggings(new ItemStackBuilder(Material.GOLDEN_LEGGINGS).build());
        this.armorStand.setBoots(new ItemStackBuilder(Material.GOLDEN_BOOTS).build());
        this.armorStand.setLeftArmPose(new EulerAngle(30.0, 90.0, 85.0));
        this.armorStand.setRightArmPose(new EulerAngle(-30.0, 0.0, 0.0));

        //Animate arm
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
            if(!Bukkit.getOnlinePlayers().isEmpty()) {
                if (this.currentState) {
                    this.armorStand.setLeftArmPose(this.ANGLE_1);
                }
                else {
                    this.armorStand.setLeftArmPose(this.ANGLE_2);
                }
                this.currentState = !this.currentState;
            }
        }, 0L, 5L);
    }

}
