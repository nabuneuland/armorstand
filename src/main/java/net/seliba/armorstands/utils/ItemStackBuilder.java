package net.seliba.armorstands.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class ItemStackBuilder {

    private ItemStack itemStack;

    public ItemStackBuilder(final Material material) {
        this.itemStack = new ItemStack(material);
    }

    public ItemStackBuilder setOwner(final String name) {
        if (this.itemStack.getType() == Material.PLAYER_HEAD) {
            final SkullMeta skullMeta = (SkullMeta) this.itemStack.getItemMeta();
            skullMeta.setOwner(name);
            this.itemStack.setItemMeta(skullMeta);
        }
        return this;
    }

    public ItemStack build() {
        return this.itemStack;
    }

}
